package com.orionark 
{
	import flash.display.Sprite;
  import flash.events.MouseEvent;
  import flash.text.TextField;
  import flash.text.TextFieldAutoSize;
  import flash.text.TextFormat;
	
	/**
   * ...
   * @author 
   */
  public class MenuButton extends Sprite 
  {
    private var m_name:String;
    private var m_callback:Function;
    private var m_callbackParameters:Array;
    
    private var m_text:TextField;
    
    public function MenuButton(name:String, callback:Function, callbackParameters:Array = null ) 
    {
      m_name = name;
      m_callback = callback;
      m_callbackParameters = callbackParameters;
      
      graphics.beginFill(0x000000);
      graphics.drawRect(0, 0, 200, 50);
      graphics.endFill();
      
      m_text = new TextField();
      var format:TextFormat = new TextFormat("Arial", 14, 0xFFFFFF, true);
      m_text.defaultTextFormat = format;
      m_text.autoSize = TextFieldAutoSize.LEFT;
      m_text.selectable = false;
      m_text.text = m_name;
      addChild(m_text);
      m_text.x = 200 / 2 - ( m_text.width / 2 );
      m_text.y = 50 / 2 - ( m_text.height / 2 );
      m_text.mouseEnabled = false;
      
      buttonMode = true;
      
      addEventListener(MouseEvent.CLICK, mouseClicked);
    }
    
    private function mouseClicked(e:MouseEvent):void
    {
      m_callback.apply(null, m_callbackParameters);
    }
  }

}