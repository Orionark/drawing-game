package com.orionark
{
	import flash.display.Sprite;
  import flash.events.Event;
	
	/**
	 * ...
	 * @author 
	 */
	public class Main extends Sprite 
	{
    private var m_game:GameType = null;
    
		private var m_buttons:Vector.<MenuButton> = new Vector.<MenuButton>();
    
		public function Main():void 
		{
			createButton("Specific Lines", SpecificLines);
      createButton("Ellipses", Ellipses);
      
      stage.addEventListener(Event.RESIZE, stageResized);
		}
    
    private function createButton(name:String, type:Class):void
    {
      var button:MenuButton = new MenuButton(name, loadGame, [type]);
      addChild(button);
      button.x = 50;
      button.y = 50 + m_buttons.length * 60;
      m_buttons.push(button);
    }
		
    private function loadGame(type:Class):void
    {
      if ( m_game )
      {
        removeChild(Sprite(m_game));
        m_game = null;
      }
      
      m_game = new type();
      addChild(Sprite(m_game));
      m_game.start(closeGame);
      
    }
    
    private function closeGame():void
    {
      if ( m_game )
      {
        removeChild(Sprite(m_game));
        m_game = null;
      }
    }
    
    private function stageResized(e:Event):void
    {
      if ( m_game )
      {
        m_game.resize();
      }
    }
	}
	
}