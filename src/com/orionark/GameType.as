package com.orionark 
{
	/**
   * ...
   * @author 
   */
  public interface GameType 
  {
    function start(callback:Function):void;
    function resize():void;
  }

}