package com.orionark 
{
	import flash.display.Sprite;
  import flash.events.Event;
  import flash.events.MouseEvent;
  import flash.geom.Point;
  import flash.text.TextField;
  import flash.text.TextFieldAutoSize;
  import flash.text.TextFormat;
	
	/**
   * ...
   * @author 
   */
  public class SpecificLines extends Sprite implements GameType
  {
    private var m_background:Sprite;
    
    private var m_dot1:Sprite;
    private var m_dot2:Sprite;
    
    private var m_start:Point = new Point();
    private var m_end:Point = new Point();
    private var m_actualDistance:Number = 0;
    private var m_actualScore:Number = 0;
    
    private var m_started:Number;
    
    private var m_again:MenuButton;
    private var m_new:MenuButton;
    private var m_menu:MenuButton;
    
    private var m_results:TextField;
    
    private var m_source:Sprite;
    private var m_target:Sprite;
    
    private var m_samples:Vector.<Point> = new Vector.<Point>();
    
    private var m_resultLine:Sprite;
    
    private var m_distance:Number = 0;
    private var m_score:Number = 0;
    
    private var m_endCallback:Function;
    
    public function SpecificLines() 
    {
      addEventListener(Event.ADDED_TO_STAGE, initialize);
    }
    
    private function initialize(e:Event):void
    {
      m_background = new Sprite();
      
      m_background.graphics.beginFill(0xFFFFFF);
      m_background.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
      m_background.graphics.endFill();
      
      addChild(m_background);
      
      m_dot1 = new Sprite();
      m_dot1.graphics.beginFill(0x00FF00);
      m_dot1.graphics.drawCircle(0, 0, 4);
      m_dot1.graphics.endFill();
      addChild(m_dot1);
      
      m_dot2 = new Sprite();
      m_dot2.graphics.beginFill(0xFF0000);
      m_dot2.graphics.drawCircle(0, 0, 4);
      m_dot2.graphics.endFill();
      addChild(m_dot2);
      
      m_again = new MenuButton("Try Again", resetLine);
      m_menu = new MenuButton("Back to Menu", mainMenu);
      m_new = new MenuButton("New Line", configureDots);
      
      m_results = new TextField();
      var format:TextFormat = new TextFormat("Arial", 14, 0x0000FF, true);
      m_results.defaultTextFormat = format;
      m_results.multiline = true;
      m_results.autoSize = TextFieldAutoSize.LEFT;
      m_results.width = 400;
      
      m_resultLine = new Sprite();
      addChild(m_resultLine);
    }
    
    private function mainMenu():void
    {
      m_endCallback();
    }
    
    public function start(callback:Function):void
    {
      m_endCallback = callback;
      configureDots();
    }
    
    private function configureDots():void
    {      
      m_dot1.x = Math.round(Math.random() * (stage.stageWidth - 100)) + 50;
      m_dot1.y = Math.round(Math.random() * (stage.stageHeight - 100)) + 50;
      
      m_dot2.x = Math.round(Math.random() * (stage.stageWidth - 100)) + 50;
      m_dot2.y = Math.round(Math.random() * (stage.stageHeight - 100)) + 50;
      
      resetLine();
    }
    
    private function resetLine():void
    {
      if ( contains(m_again) )
      {
        removeChild(m_again);
        removeChild(m_menu);
        removeChild(m_new);
        removeChild(m_results);
      }
      
      m_start.x = m_dot1.x;
      m_start.y = m_dot1.y;
      
      m_end.x = m_dot2.x;
      m_end.y = m_dot2.y;
      
      m_actualDistance = Math.sqrt(dist2(m_start, m_end));
      
      stage.addEventListener(MouseEvent.MOUSE_DOWN, stageDown);
      
      m_resultLine.graphics.clear();
    }
    
    private function stageDown(e:MouseEvent):void
    {
      m_distance = 0;
      m_score = 0;
      m_actualScore = 0;
      m_started = (new Date()).time;
      stage.removeEventListener(MouseEvent.MOUSE_DOWN, stageDown);
      stage.addEventListener(MouseEvent.MOUSE_MOVE, stageMove);
      stage.addEventListener(MouseEvent.MOUSE_UP, stageUp);
      
      m_samples.length = 0;
      m_samples.push(new Point(mouseX, mouseY));
      m_resultLine.graphics.lineStyle(1, 0, 1);
      m_resultLine.graphics.moveTo(mouseX, mouseY);
    }
    
    private function stageMove(e:MouseEvent):void
    {
      var point:Point = new Point(mouseX, mouseY);
      var distance:Number = distToSegment(point, m_start, m_end);
      var percent:Number = Math.min(distance / 10, 1);
      m_actualScore += 1;
      m_score += 1 - percent;
      var color:uint = ((0xFF * percent) << 16) | ((0xFF * ( 1 - percent )) << 8);
      m_resultLine.graphics.lineStyle(1, color);
      m_resultLine.graphics.lineTo(mouseX, mouseY);
      var lineDistance:Number = Math.sqrt(dist2(m_samples[m_samples.length - 1], point));
      m_distance += lineDistance;
      m_samples.push(point);
    }
    
    private function stageUp(e:MouseEvent):void
    {
      m_samples.push(new Point(mouseX, mouseY));
      
      var totalDistance:Number = 0;
      var current:uint = 0;
      for each ( var point:Point in m_samples )
      {        
        totalDistance += distToSegment(point, m_start, m_end);
        
        current++;
      }
      
      var averageDistance:Number = totalDistance / current;
      
      var totalVariance:Number = 0;
      for each ( point in m_samples )
      {
        var distance:Number = distToSegment(point, m_start, m_end);
        totalVariance += Math.pow(distance - averageDistance, 2);
      }
      
      var averageVariance:Number = totalVariance / current;
      var standardDeviation:Number = Math.sqrt(averageVariance);
      
      var straightScore:Number = 100 - ( standardDeviation * 10 );
      straightScore = m_score / m_actualScore * 100;
      var lengthScore:Number = 0;
      if ( m_actualDistance > m_distance )
      {
        lengthScore = ( m_distance / m_actualDistance ) * 100;
      }
      else
      {
        lengthScore = ( m_actualDistance / m_distance ) * 100;
      }
      if ( straightScore < 0 )
      {
        straightScore = 0;
      }
      else if ( straightScore > 100 )
      {
        straightScore = 100;
      }
      if ( lengthScore < 0 )
      {
        lengthScore = 0;
      }
      
      var scoreAverage:Number = ( straightScore + lengthScore ) / 2;
      m_results.text = "Your results:\n";
      
      addScore("Accuracy", straightScore);
      addScore("Length", lengthScore);
      addScore("Overall", scoreAverage);
      
      m_resultLine.graphics.lineStyle(1, 0xFF0000, 1);
      m_resultLine.graphics.moveTo(m_dot1.x, m_dot1.y);
      m_resultLine.graphics.lineTo(m_dot2.x, m_dot2.y);
      
      stage.removeEventListener(MouseEvent.MOUSE_MOVE, stageMove);
      stage.removeEventListener(MouseEvent.MOUSE_UP, stageUp);
      
      addChild(m_again);
      addChild(m_menu);
      addChild(m_new);
      m_new.x = stage.stageWidth - m_new.width - 50;
      m_new.y = stage.stageHeight - m_new.height - 50;
      m_again.x = m_new.x;
      m_again.y = m_new.y - m_again.height - 5;
      m_menu.x = m_again.x;
      m_menu.y = m_again.y - m_menu.height - 5;
      
      addChild(m_results);
      m_results.x = 50;
      m_results.y = 50;
    }
    
    private function addScore(name:String, score:Number):void
    {
      m_results.appendText("\n" + name + ": " + score.toFixed(1));
      
      if ( score >= 94 )
      {
        m_results.appendText(" A+");
      }
      else if ( score >= 80 )
      {
        m_results.appendText(" A");
      }
      else if ( score >= 70 )
      {
        m_results.appendText(" B");
      }
      else if ( score >= 60 )
      {
        m_results.appendText(" C");
      }
      else if ( score >= 50 )
      {
        m_results.appendText(" D");
      }
      else
      {
        m_results.appendText(" F");
      }
    }
    
    private function dist2(v:Point, w:Point):Number
    { 
      return Math.pow(v.x - w.x, 2) + Math.pow(v.y - w.y, 2) 
    }
    
    private function distToSegmentSquared(p:Point, v:Point, w:Point):Number
    {
      var l2:Number = dist2(v, w);
      if (l2 == 0) return dist2(p, v);
      var t:Number = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;
      return dist2(p, new Point(v.x + t * (w.x - v.x), v.y + t * (w.y - v.y)));
    }
    
    private function distToSegment(p:Point, v:Point, w:Point):Number
    { 
      return Math.sqrt(distToSegmentSquared(p, v, w)); 
    }
    
    public function resize():void
    {
      
    }
  }

}